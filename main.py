import subprocess
import pprint

outputFile = open("gsettings-values", "a")
listOfSchemas = subprocess.check_output("gsettings list-schemas".split(), universal_newlines=True).splitlines()

for schema in listOfSchemas:
    command = "gsettings list-recursively " + str(schema)
    listOfValues = subprocess.check_output(command.split(), universal_newlines=True).splitlines()
    temp = pprint.pformat(listOfValues)
    print(temp, end="", file=outputFile)
